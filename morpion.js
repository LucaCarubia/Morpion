//---------------------------------------TABLEAU ET SES COLONNES-------------------------------------------------------

var array = [
    [0,0,0], //Colonne 1
    [0,0,0], //Colonne 2
    [0,0,0] //Colonne 3
];

//----------------------------------------------VARIABLES VIDE A APPELÉES------------------------------------------------

// var array = [0,1];
var cases = 0;
var tours = 0;
var player1Name = "";
var player2Name = "";
var changePlayer = "";

//-------------------------------------RECUPÉRATION DES NOMS DES JOUEURS INPUT--------------------------------------------

function recupNomsJoueurs() {
    player1Name = document.getElementsByClassName('player1Name').value;
    player2Name = document.getElementsByClassName('player2Name').value;
}

//------------------------------------ RECUPÉRATION DES COLONNES DU TABLEAU----------------------------------------------//

function colTab(numCol) {
    boxes = document.getElementsByClassName("col-" + numCol);
    colTab(numCol);
}

//---------------------------------------INSERE L'ELEMENT CROIX ET ROND----------------------------------------------//

function insert(line, colum) {
    // document <html>
    // box <td> 
    var box = document.querySelector('.pos-' + line + '-' + colum);
    var hasDiv = box.querySelector('div'); // de type Element

    // si y'a deja un div on sort
    if (hasDiv) { return; }

    if (changePlayer == '<div class="croix"></div>') {
        changePlayer = '<div class="rond"></div>';
    } else {
        changePlayer = '<div class="croix"></div>';
    }
    box.innerHTML = changePlayer;
    isWinHorizontal();
    isWinVertical();
}

//----------------------------------------------------VICTOIRE EN HORIZONTAL-------------------------------------------//

 function isWinHorizontal() {

     //----------------------------------------------PREMIERE LIGNE------------------------------------------------------//

    var joueurLigne1 = "";
    if (changePlayer == '<div class="croix"></div>') {
        joueurLigne1 = 'croix';
    } else {
        joueurLigne1 = 'rond';
    }

    var box1 = document.querySelector('.pos-' + 0 + '-' + 0);
    var box2 = document.querySelector('.pos-' + 0 + '-' + 1);
    var box3 = document.querySelector('.pos-' + 0 + '-' + 2);


       if (box1.querySelector('div .'+ joueurLigne1) && box2.querySelector('div .'+ joueurLigne1) 
       && box3.querySelector('div .'+ joueurLigne1))
   {
           alert('Le ' + joueurLigne1 + " a gagner"); 
   }

//--------------------------------------------DEUXIEME LIGNE------------------------------------------------------//

   
    var joueurLigne2 = "";
    if (changePlayer == '<div class="croix"></div>') {
        joueurLigne2 = 'croix';
    } else {
        joueurLigne2 = 'rond';
    }

    var box4 = document.querySelector('.pos-' + 1 + '-' + 0);
    var box5 = document.querySelector('.pos-' + 1 + '-' + 1);
    var box6 = document.querySelector('.pos-' + 1 + '-' + 2);

    // deuxième ligne, victoire 
    if (box4.querySelector('div .'+ joueurLigne2) && box5.querySelector('div .'+ joueurLigne2) 
    && box6.querySelector('div .'+ joueurLigne2))
{
        alert('Le ' + joueurLigne2 + " a gagner"); 
}

//--------------------------------------------TROISIEME LIGNE------------------------------------------------------//


    var joueurLigne3 = "";
    if (changePlayer == '<div class="croix"></div>') {
        joueurLigne3 = 'croix';
    } else {
        joueurLigne3 = 'rond';
    }

    var box7 = document.querySelector('.pos-' + 2 + '-' + 0);
    var box8 = document.querySelector('.pos-' + 2 + '-' + 1);
    var box9 = document.querySelector('.pos-' + 2 + '-' + 2);
    
    // troisième ligne, victoire    
    if (box7.querySelector('div .'+ joueurLigne3) && box8.querySelector('div .'+ joueurLigne3) 
    && box9.querySelector('div .'+ joueurLigne3))
    {
        alert('Le ' + joueurLigne3 + " a gagner"); 
    }

}

//-------------------------------------VICTOIRE EN VERTICAL------------------------------------------------------------//

function isWinVertical() {

//----------------------------------------PREMIERE COLONNE-------------------------------------------------------//

    var joueurColonne1 = "";
    if (changePlayer == '<div class="croix"></div>') {
        joueurColonne1 = 'croix';
    } else {
        joueurColonne1 = 'rond';
    }

    var box1 = document.querySelector('.pos-' + 0 + '-' + 0);
    var box4 = document.querySelector('.pos-' + 1 + '-' + 0);
    var box7 = document.querySelector('.pos-' + 2 + '-' + 0);
    

    if (box1.querySelector('div .'+ joueurColonne1) && box4.querySelector('div .'+ joueurColonne1) 
    && box7.querySelector('div .'+ joueurColonne1))
    {
        alert('Le ' + joueurColonne1 + " a gagner"); 
    }

    //---------------------------------------DEUXIEME COLONNE------------------------------------------------------------//

    var joueurColonne2 = "";
    if (changePlayer == '<div class="croix"></div>') {
        joueurColonne2 = 'croix';
    } else {
        joueurColonne2 = 'rond';
    }

    var box2 = document.querySelector('.pos-' + 0 + '-' + 1);
    var box5 = document.querySelector('.pos-' + 1 + '-' + 1);
    var box8 = document.querySelector('.pos-' + 2 + '-' + 1);
    

    if (box2.querySelector('div .'+ joueurColonne2) && box5.querySelector('div .'+ joueurColonne2) 
    && box8.querySelector('div .'+ joueurColonne2))
    {
        alert('Le ' + joueurColonne2 + " a gagner"); 
    }

//---------------------------------------TROISIÈME COLONNE------------------------------------------------------------//

        var joueurColonne3 = "";
        if (changePlayer == '<div class="croix"></div>') {
            joueurColonne3 = 'croix';
        } else {
            joueurColonne3 = 'rond';
        }
    
        var box3 = document.querySelector('.pos-' + 0 + '-' + 2);
        var box6 = document.querySelector('.pos-' + 1 + '-' + 2);
        var box9 = document.querySelector('.pos-' + 2 + '-' + 2);
        
    
        if (box3.querySelector('div .'+ joueurColonne3) && box6.querySelector('div .'+ joueurColonne3) 
        && box9.querySelector('div .'+ joueurColonne3))
        {
            alert('Le ' + joueurColonne3 + " a gagner"); 
        }
}

//---------------------------------------VICTOIRE EN DIAGONAL---------------------------------------------------------//